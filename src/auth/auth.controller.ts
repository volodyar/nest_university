import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Res,
  UseFilters,
  UsePipes,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';
import { AuthService } from './auth.service';
import { AuthGuard, AuthRefreshTokenGuard } from './guard';
import { QueryFailedErrorFilter } from '../application/exception';
import {
  ApiOperation,
  ApiUnauthorizedResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiTags,
  ApiCookieAuth,
  ApiNoContentResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import {
  AuthRequestDto,
  AuthResponseDto,
  RefreshTokenResponseDto,
  ResetPasswordRequestDto,
  ResetPasswordWithTokenRequestDto,
} from './dto';
import { Cookie, User } from './decorator';
import { TrimPipe } from '../application/pipes';

@ApiTags('Auth')
@UseFilters(QueryFailedErrorFilter)
@Controller('api/v1/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Sign up' })
  @ApiCreatedResponse({
    description: 'Created',
  })
  @ApiBadRequestResponse({ description: 'Bad Request | Validation error' })
  @ApiForbiddenResponse({ description: 'Forbidden | Credentials taken' })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(new TrimPipe())
  @Post('signup')
  async signup(@Body() dto: AuthRequestDto): Promise<void> {
    await this.authService.signup(dto);
  }

  @ApiOperation({ summary: 'Sign in' })
  @ApiOkResponse({
    status: HttpStatus.OK,
    type: AuthResponseDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({ description: 'Bad Request | Validation error' })
  @ApiForbiddenResponse({
    description: 'Forbidden | Invalid email or password',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @HttpCode(HttpStatus.OK)
  @Post('signin')
  async signin(
    @Body() dto: AuthRequestDto,
    @Res({ passthrough: true }) res: Response,
  ): Promise<Omit<AuthResponseDto, 'refresh_token'>> {
    const { access_token, refresh_token, email } =
      await this.authService.signin(dto);

    res.cookie('refresh_token', refresh_token, {
      httpOnly: true,
      path: '/',
      signed: true,
      secure: true,
      sameSite: 'none',
      expires: new Date(Date.now() + 60 * 60 * 24 * 8 * 1000),
    });

    return { access_token, email };
  }

  @ApiOperation({ summary: 'Log out' })
  @ApiCookieAuth()
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized exception',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('logout')
  async logout(
    @Res({ passthrough: true }) res: Response,
    @User('sub') id: number,
  ): Promise<void> {
    await this.authService.logout(id);
    res.clearCookie('refresh_token', {
      httpOnly: true,
      path: '/',
      signed: true,
      secure: true,
      sameSite: 'none',
    });
  }

  @ApiOperation({ summary: 'Get reset password token' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiForbiddenResponse({
    description: 'Forbidden | User not found',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('reset-password-request')
  async resetPasswordRequest(
    @Body() dto: ResetPasswordRequestDto,
  ): Promise<void> {
    await this.authService.resetPasswordRequest(dto);
  }

  @ApiOperation({ summary: 'Reset password' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiBadRequestResponse({
    description:
      'Bad Request | There no registered reset password operation by the token',
  })
  @ApiForbiddenResponse({
    description: 'Forbidden | User not found',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('reset-password')
  async resetPassword(
    @Body() dto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    await this.authService.resetPassword(dto);
  }

  @ApiOperation({ summary: 'Refresh token' })
  @ApiCookieAuth()
  @ApiOkResponse({
    description: 'OK',
    type: RefreshTokenResponseDto,
  })
  @ApiBadRequestResponse({ description: 'Bad Request | Validation error' })
  @ApiForbiddenResponse({
    description: 'Forbidden | Access Denied',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @UseGuards(AuthRefreshTokenGuard)
  @HttpCode(HttpStatus.OK)
  @Get('refresh-token')
  async refreshToken(
    @User('sub') id: number,
    @Cookie('refresh_token') rt: string,
  ): Promise<RefreshTokenResponseDto> {
    const { access_token } = await this.authService.refreshToken(id, rt);

    return { access_token };
  }
}
