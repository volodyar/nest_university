import { IsEmail, IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordRequestDto {
  @ApiProperty({ default: 'johnsmith@gmail.com' })
  @IsEmail()
  @IsNotEmpty()
  @Length(4)
  email: string;
}
