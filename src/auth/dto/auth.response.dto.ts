import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class AuthResponseDto {
  @ApiProperty({ default: 'johnsmith@gmail.com' })
  email: string;

  @ApiProperty({ default: 'John Smith' })
  @IsOptional()
  name?: string;

  @ApiProperty({
    default:
      'eyJhbGciOiJIUzI1NiIsInQ3cCI6IkpXVCJ9.eyJzdWIiOjEsImVtYWlsIjoidm9sb2RpYS5pYXJjZ3VrQGdtYWlsLmNvbSIsImlhdCI6MTY5MzI5MjAxOSwiZXhwIjoxNjkzMjkyOTE5fQ.VegvNAFDVlohT6Y7la3xHxw1xB5J0FBorVKaOg8l8g0',
  })
  access_token: string;

  @ApiProperty({ deprecated: true })
  refresh_token: string;
}
