import { IsString, IsNotEmpty, Length, IsBase64 } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordWithTokenRequestDto {
  @ApiProperty({ default: '123456789' })
  @IsString()
  @IsNotEmpty()
  @Length(6, 16)
  password: string;

  @ApiProperty({
    default: '52a6633cd6608583b4ac3f7865a5ba6fe457078cdef4c64d326375a0222197b5',
  })
  @IsNotEmpty()
  @IsString()
  @IsBase64()
  token: string;
}
