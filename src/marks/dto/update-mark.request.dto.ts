import { PartialType } from '@nestjs/swagger';
import { CreateMarkRequestDto } from './create-mark.request.dto';

export class UpdateMarkRequestDto extends PartialType(CreateMarkRequestDto) {}
