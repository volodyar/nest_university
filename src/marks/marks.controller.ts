import {
  Body,
  Controller,
  HttpStatus,
  Post,
  UseFilters,
  UseGuards,
  Delete,
  Patch,
  Param,
  HttpCode,
  UsePipes,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiTags,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiCookieAuth,
  ApiUnauthorizedResponse,
  ApiOperation,
  ApiNoContentResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { QueryFailedErrorFilter } from '../application/exception';
import { AuthGuard } from '../auth/guard';
import { MarksService } from './marks.service';
import {
  CreateMarkResponseDto,
  CreateMarkRequestDto,
  UpdateMarkRequestDto,
} from './dto';
import { TrimPipe } from '../application/pipes';

@ApiTags('Marks')
@ApiCookieAuth()
@UseFilters(QueryFailedErrorFilter)
@UseGuards(AuthGuard)
@Controller('api/v1/marks')
export class MarksController {
  constructor(private readonly marksService: MarksService) {}

  @ApiOperation({ summary: 'Add the mark to the student' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: CreateMarkResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad Request | Credentials taken | Validation Error',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @UsePipes(new TrimPipe())
  @Post('/')
  async create(
    @Body() dto: CreateMarkRequestDto,
  ): Promise<CreateMarkResponseDto> {
    return await this.marksService.create(dto);
  }

  @ApiOperation({ summary: "Update the mark's data" })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiNotFoundResponse({ description: 'Not Found | Mark not found' })
  @ApiBadRequestResponse({
    description: "Bad Request | Credentials don't exist | Validation error",
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Patch(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async patch(
    @Param('id') id: number,
    @Body() dto: UpdateMarkRequestDto,
  ): Promise<void> {
    return await this.marksService.patch(id, dto);
  }

  @ApiOperation({ summary: 'Delete the mark' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Not Found | Mark not found' })
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id') id: number): Promise<void> {
    return await this.marksService.delete(id);
  }
}
