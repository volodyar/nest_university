import { MailerService } from '@nestjs-modules/mailer';
import { join } from 'path';
import { Injectable } from '@nestjs/common';
import { Lector } from '../lectors/entities/lector.entity';

@Injectable({})
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendResetPassword(user: Lector, token: string): Promise<void> {
    const url = `http://localhost:3000/hw_university_front/reset-password?token=${token}`;

    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Welcome to University App! Reset your Password',
      template: join(__dirname, 'templates', 'reset-password'),
      context: {
        name: user.name || 'lector',
        url,
      },
    });
  }
}
