import { CreateGroupResponseDto } from './create-group.response.dto';

export class UpdateGroupResponseDto extends CreateGroupResponseDto {}
