import { ApiProperty } from '@nestjs/swagger';
import { CreateGroupResponseDto } from './create-group.response.dto';

export class FindAllGroupsResponseDto {
  @ApiProperty({ type: [CreateGroupResponseDto] })
  groups: CreateGroupResponseDto[];

  @ApiProperty({ default: 1 })
  count: number;
}
