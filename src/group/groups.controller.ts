import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
  HttpCode,
  UseGuards,
  UseFilters,
  Query,
  UsePipes,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiTags,
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCookieAuth,
  ApiUnauthorizedResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { AuthGuard } from '../auth/guard';
import { QueryFailedErrorFilter } from '../application/exception';
import { GroupsService } from './groups.service';
import {
  CreateGroupRequestDto,
  CreateGroupResponseDto,
  FindAllGroupsResponseDto,
  UpdateGroupRequestDto,
  UpdateGroupResponseDto,
} from './dto';
import { QueryFilterDto } from '../application/dto';
import { TrimPipe } from '../application/pipes';

@ApiCookieAuth()
@ApiTags('Groups')
@UseGuards(AuthGuard)
@UseFilters(QueryFailedErrorFilter)
@Controller('api/v1/groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @ApiOperation({ summary: 'Create the group' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: CreateGroupResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad Request | Credentials taken | Validation Error',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @UsePipes(new TrimPipe())
  @Post('/')
  async create(
    @Body() dto: CreateGroupRequestDto,
  ): Promise<CreateGroupResponseDto> {
    return await this.groupsService.create(dto);
  }

  @ApiOperation({ summary: 'Get an array of groups' })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: FindAllGroupsResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/')
  async findAll(
    @Query() query: QueryFilterDto,
  ): Promise<FindAllGroupsResponseDto> {
    return await this.groupsService.findAll(query);
  }

  @ApiOperation({ summary: 'Get the group' })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: CreateGroupResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get(':id')
  async findOne(
    @Param('id') id: number,
  ): Promise<CreateGroupResponseDto | object> {
    return await this.groupsService.findOne(id);
  }

  @ApiOperation({ summary: "Update the group's data" })
  @ApiOkResponse({ type: UpdateGroupResponseDto })
  @ApiNotFoundResponse({ description: 'Not Found | Group not found' })
  @ApiBadRequestResponse({
    description: "Bad Request | Credentials don't exist | Validation error",
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Patch(':id')
  @HttpCode(200)
  async patch(
    @Param('id') id: number,
    @Body() dto: UpdateGroupRequestDto,
  ): Promise<UpdateGroupResponseDto> {
    return await this.groupsService.patch(id, dto);
  }

  @ApiOperation({ summary: 'Delete the group' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Not Found | Group not found' })
  @Delete(':id')
  @HttpCode(204)
  async delete(@Param('id') id: number): Promise<void> {
    return this.groupsService.delete(id);
  }
}
