import { parse, config } from 'dotenv';
import { Injectable, Logger } from '@nestjs/common';
import { existsSync, readFileSync } from 'fs';
import { resolve } from 'path';
import { validateSync, ValidationError } from 'class-validator';

import { ConfigDto } from './dto/config.dto';
import { AppEnvs } from '../application/enums';

const ROOT_PATH = process.cwd();
const ENV_FILE = resolve(ROOT_PATH, '.env');

@Injectable()
export class EnvConfigService {
  private readonly logger = new Logger(EnvConfigService.name);
  private readonly configuration: ConfigDto;

  constructor() {
    const configuration = new ConfigDto();

    Object.assign(configuration, {
      ...EnvConfigService.getDotenvConfiguration(),
      ...process.env,
    });

    const validationResult = validateSync(configuration, {
      whitelist: true,
      forbidUnknownValues: true,
    });

    if (validationResult && validationResult.length > 0) {
      this.logger.error(
        'Configurations invalid',
        `Validation errors:\n${EnvConfigService.extractValidationErrorMessages(
          validationResult,
        )}`,
      );
      throw new Error(
        `Configurations invalid \n${validationResult.toString()}`,
      );
    }

    this.configuration = configuration;
  }

  public static getDotenvConfiguration(): Record<string, unknown> {
    let configuration = {};
    if (existsSync(ENV_FILE)) {
      configuration = parse(readFileSync(ENV_FILE));
    }
    if (configuration['NODE_ENV'] === AppEnvs.LOCAL) {
      config();
    }
    return configuration;
  }

  public static extractValidationErrorMessages(
    validationErrors: ValidationError[],
  ): string {
    return validationErrors
      .map(
        (validationError) =>
          `${Object.values(validationError.constraints)
            .map((constraint) => `  * ${constraint}.`)
            .join('\n')}`,
      )
      .join('.\n');
  }

  public get<K extends keyof ConfigDto>(key: K): ConfigDto[K] {
    if (!this.configuration[key] || this.configuration[key] === 'null') {
      return;
    }
    return this.configuration[key];
  }
}
