import { DataSource, Repository } from 'typeorm';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import {
  BadRequestException,
  HttpException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { extname, join } from 'path';
import { rename } from 'fs/promises';
import { unlink } from 'fs';
import ObjectID from 'bson-objectid';
import { Student } from './entities/students.entity';
import {
  CreateStudentRequestDto,
  CreateStudentResponseDto,
  StudentCoursesResponseDto,
  StudentMarksResponseDto,
  UpdateStudentRequestDto,
  addStudentCourseRequestDto,
  FindAllStudentsResponseDto,
  UploadStudenPhotoResponseDto,
  UpdateStudentResponseDto,
} from './dto';
import { QueryFilterDto } from '../application/dto';

@Injectable({})
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentsRepository: Repository<Student>,
    @InjectDataSource() private readonly dataSourse: DataSource,
  ) {}

  async create(
    dto: CreateStudentRequestDto,
  ): Promise<Omit<CreateStudentResponseDto, 'group'>> {
    return await this.studentsRepository.save(dto);
  }

  async findAll(query: QueryFilterDto): Promise<FindAllStudentsResponseDto> {
    const { limit, offset, sort, order, name, surname, group, course } = query;
    const page = (offset - 1) * limit;

    const queryBuilder = this.studentsRepository
      .createQueryBuilder('students')
      .select('students')
      .leftJoin('students.group', 'group')
      .addSelect('group.name')
      .leftJoinAndSelect('students.courses', 'courses');

    if (name) {
      queryBuilder.where('students.name ilike :name', {
        name: `%${name ? name : ''}%`,
      });
    }

    if (surname) {
      console.log({ surname });
      queryBuilder.where('students.surname ilike :surname', {
        surname: `%${surname ? surname : ''}%`,
      });
    }

    if (group) {
      console.log({ group });
      queryBuilder.where('group.name ilike :group', {
        group: `%${group ? group : ''}%`,
      });
    }

    if (course) {
      console.log({ course });
      queryBuilder.where('courses.name ilike :course', {
        course: `%${course ? course : ''}%`,
      });
    }

    const [students, count] = await queryBuilder
      .orderBy(`students.${sort}`, `${order}`)
      .skip(page)
      .take(limit)
      .getManyAndCount();

    return { students, count };
  }

  async findOne(id: number): Promise<CreateStudentResponseDto | object> {
    const student = await this.studentsRepository
      .createQueryBuilder('student')
      .select('student')
      .leftJoin('student.group', 'group')
      .addSelect('group.name')
      .where('student.id = :id', { id })
      .getOne();

    if (!student) return {};

    return student;
  }

  async findOneCourses(
    id: number,
  ): Promise<StudentCoursesResponseDto | object> {
    const student = await this.studentsRepository
      .createQueryBuilder('student')
      .select('student')
      .leftJoin('student.group', 'group')
      .addSelect('group.name')
      .leftJoinAndSelect('student.courses', 'courses')
      .where('student.id = :id', { id })
      .getOne();

    if (!student) return {};

    return student;
  }

  async addCourse(dto: addStudentCourseRequestDto): Promise<void> {
    await this.dataSourse
      .createQueryBuilder()
      .insert()
      .into('student_course')
      .values({ student_id: dto.studentId, course_id: dto.courseId })
      .execute();
  }

  async findOneMarks(id: number): Promise<StudentMarksResponseDto | object> {
    const student = await this.studentsRepository
      .createQueryBuilder('student')
      .select(['student', 'marks.mark', 'course.name'])
      .leftJoin('student.group', 'group')
      .addSelect('group.name')
      .leftJoin('student.marks', 'marks')
      .leftJoin('marks.course', 'course')
      .where('student.id = :id', { id })
      .getOne();

    if (!student) return {};

    return student;
  }

  async patch(
    id: number,
    dto: UpdateStudentRequestDto,
  ): Promise<UpdateStudentResponseDto> {
    const result = await this.studentsRepository.update(id, dto);

    if (!result.affected) throw new NotFoundException('Student not found');

    return await this.studentsRepository.findOneBy({ id });
  }

  async delete(id: number): Promise<void> {
    const result = await this.studentsRepository.delete(id);

    if (!result.affected) throw new NotFoundException('Student not found');
  }

  async uploadPhoto(
    file: Express.Multer.File,
    id: number,
  ): Promise<UploadStudenPhotoResponseDto> {
    const user = await this.studentsRepository.findBy({ id });

    if (!user) {
      throw new NotFoundException('Student not found');
    }
    const { path: filePath } = file;

    if (!filePath) {
      throw new BadRequestException('File is not provided');
    }

    try {
      const imageId = ObjectID().toHexString();
      const imageExtention = extname(filePath);
      const imageName = imageId + imageExtention;

      const studentsDirectory = 'students';
      const studentsDirectoryPath = join(
        __dirname,
        '..',
        'public',
        studentsDirectory,
      );
      const newImagePath = join(studentsDirectoryPath, imageName);
      const imagePath = join(studentsDirectory, imageName);

      await rename(filePath, newImagePath);
      const res = await this.studentsRepository.update(id, { imagePath });

      if (res.affected) {
        return { imagePath };
      }
    } catch (error: unknown) {
      unlink(filePath, (err) => {
        if (err) console.log(err);
      });
      const err = error as { message: string; status: number };
      throw new HttpException(err.message, err.status);
    }
  }
}
