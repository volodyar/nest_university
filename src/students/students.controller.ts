import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UseFilters,
  UsePipes,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  ParseFilePipe,
  MaxFileSizeValidator,
  FileTypeValidator,
} from '@nestjs/common';
import { diskStorage } from 'multer';
import { join } from 'path';
import {
  ApiCreatedResponse,
  ApiTags,
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCookieAuth,
  ApiUnauthorizedResponse,
  ApiOperation,
  ApiBody,
  ApiConsumes,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { StudentsService } from './students.service';
import { QueryFailedErrorFilter } from '../application/exception';
import { AuthGuard } from '../auth/guard';
import {
  CreateStudentRequestDto,
  CreateStudentResponseDto,
  StudentCoursesResponseDto,
  StudentMarksResponseDto,
  UpdateStudentRequestDto,
  addStudentCourseRequestDto,
  FindAllStudentsResponseDto,
  UploadStudenPhotoResponseDto,
  UpdateStudentResponseDto,
} from './dto';
import { QueryFilterDto } from '../application/dto';
import { TrimPipe } from '../application/pipes';

@ApiCookieAuth()
@ApiTags('Students')
@UseGuards(AuthGuard)
@UseFilters(QueryFailedErrorFilter)
@Controller('api/v1/students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @ApiOperation({ summary: 'Create the student' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: CreateStudentResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad Request | Credentials taken | Validation Error',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @UsePipes(new TrimPipe())
  @Post('/')
  async create(
    @Body() student: CreateStudentRequestDto,
  ): Promise<Omit<CreateStudentResponseDto, 'group'>> {
    return await this.studentsService.create(student);
  }

  @ApiOperation({ summary: 'Get an array of students' })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: FindAllStudentsResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/')
  async findAll(
    @Query() query: QueryFilterDto,
  ): Promise<FindAllStudentsResponseDto> {
    return await this.studentsService.findAll(query);
  }

  @ApiOperation({ summary: 'Add course to the student' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiBadRequestResponse({
    description: "Bad Request | Credentials taken | Credentials don't exist",
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Post('/add-course')
  @HttpCode(HttpStatus.NO_CONTENT)
  addCourse(@Body() dto: addStudentCourseRequestDto): Promise<void> {
    return this.studentsService.addCourse(dto);
  }

  @ApiOperation({ summary: "Get the student's courses" })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: StudentCoursesResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/courses')
  findOneCourses(
    @Query('student') id: number,
  ): Promise<StudentCoursesResponseDto | object> {
    return this.studentsService.findOneCourses(id);
  }

  @ApiOperation({ summary: "Get the student's marks" })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: StudentMarksResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/marks')
  async findOneMarks(
    @Query('student') id: number,
  ): Promise<StudentMarksResponseDto | object> {
    return await this.studentsService.findOneMarks(id);
  }

  @ApiOperation({ summary: 'Get the student' })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: CreateStudentResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get(':id')
  async finOne(
    @Param('id') id: number,
  ): Promise<CreateStudentResponseDto | object> {
    return await this.studentsService.findOne(id);
  }

  @ApiOperation({ summary: "Update the student's data" })
  @ApiOkResponse({ type: UpdateStudentResponseDto })
  @ApiNotFoundResponse({ description: 'Not Found | Student not found' })
  @ApiBadRequestResponse({
    description: "Bad Request | Credentials don't exist | Validation error",
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  async patch(
    @Param('id') id: number,
    @Body() dto: UpdateStudentRequestDto,
  ): Promise<UpdateStudentResponseDto> {
    return await this.studentsService.patch(id, dto);
  }

  @ApiOperation({ summary: 'Upload the student photo' })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: UploadStudenPhotoResponseDto,
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiBadRequestResponse({ description: 'File is not provided' })
  @ApiNotFoundResponse({ description: 'Student not found' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Patch('/upload-photo/:id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: join(__dirname, '..', 'temp', 'img'),
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
    }),
  )
  async uploadFile(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new FileTypeValidator({ fileType: '.(png|jpeg|jpg)' }),
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 4 }),
        ],
      }),
    )
    file: Express.Multer.File,
    @Param('id') id: number,
  ): Promise<UploadStudenPhotoResponseDto> {
    return await this.studentsService.uploadPhoto(file, id);
  }

  @ApiOperation({ summary: 'Delete the student' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Not Found | Student not found' })
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id') id: number): Promise<void> {
    return await this.studentsService.delete(id);
  }
}
