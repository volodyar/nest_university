import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { StudentsService } from './students.service';
import { StudentsController } from './students.controller';
import { Student } from './entities/students.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Student])],
  providers: [StudentsService, JwtService],
  controllers: [StudentsController],
})
export class StudentModule {}
