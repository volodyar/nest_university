import { CommonEntity } from '../../application/entity';
import { Group } from '../../group/entities/group.entity';
import {
  Entity,
  Column,
  Index,
  ManyToOne,
  JoinColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Mark } from '../../marks/entities/mark.entity';
import { Course } from '../../courses/entities/courses.entity';

@Entity({ name: 'students' })
export class Student extends CommonEntity {
  @Index()
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  surname: string;

  @Column({ type: 'varchar', unique: true })
  email: string;

  @Column({ type: 'integer' })
  age: number;

  @Column({ type: 'varchar', name: 'image_path', nullable: true })
  imagePath: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: false,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column({ name: 'group_id', nullable: true, type: 'int' })
  groupId: number | null;

  @ManyToMany(() => Course, { onDelete: 'CASCADE', onUpdate: 'NO ACTION' })
  @JoinTable({
    name: 'student_course',
    joinColumn: { name: 'student_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'course_id', referencedColumnName: 'id' },
  })
  courses: [Course];

  @OneToMany(() => Mark, (mark) => mark.student, { cascade: true })
  marks: Mark[];
}
