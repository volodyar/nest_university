import { ApiProperty } from '@nestjs/swagger';
import { CreateStudentResponseDto } from './create-student.response.dto';

class MarksInStudent {
  @ApiProperty({ default: 75 })
  mark: number;

  @ApiProperty({ default: { name: 'John' } })
  course: { name: string };
}

export class StudentMarksResponseDto extends CreateStudentResponseDto {
  @ApiProperty({ type: [MarksInStudent] })
  marks: [MarksInStudent];
}
