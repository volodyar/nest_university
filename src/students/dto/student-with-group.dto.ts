import { ApiProperty } from '@nestjs/swagger';
import { CreateStudentRequestDto } from './create-student.request.dto';

export class StudentWithGroupDto extends CreateStudentRequestDto {
  @ApiProperty({ default: { name: 'MS-30' } })
  group: { name: string };
}
