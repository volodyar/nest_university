import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, Min } from 'class-validator';

export class addStudentCourseRequestDto {
  @IsNumber()
  @Min(1)
  @Type(() => Number)
  @ApiProperty()
  studentId: number;

  @IsNumber()
  @Min(1)
  @Type(() => Number)
  @ApiProperty()
  courseId: number;
}
