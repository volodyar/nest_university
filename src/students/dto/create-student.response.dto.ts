import { ApiProperty } from '@nestjs/swagger';
import { CreateStudentRequestDto } from './create-student.request.dto';

export class CreateStudentResponseDto extends CreateStudentRequestDto {
  @ApiProperty({ default: null })
  group: { name: string } | null;

  @ApiProperty({ default: 1 })
  id: number;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  createdAt: Date;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  updatedAt: Date;
}
