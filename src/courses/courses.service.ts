import { Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './entities/courses.entity';
import {
  CoursesMarksResponseDto,
  CreateCourseRequestDto,
  CreateCourseResponseDto,
  FindAllCoursesResponseDto,
  UpdateCourseRequestDto,
  UpdateCourseResponseDto,
} from './dto';
import { QueryFilterDto } from '../application/dto';

@Injectable({})
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
  ) {}

  async findAll(query: QueryFilterDto): Promise<FindAllCoursesResponseDto> {
    const { order, sort, offset, limit, name } = query;
    const page = (offset - 1) * limit;

    const [courses, count] = await this.coursesRepository
      .createQueryBuilder('course')
      .select('course')
      .leftJoin('course.students', 'students')
      .loadRelationCountAndMap('course.studentsCount', 'course.students')
      .where('course.name ilike :name', { name: `%${name ? name : ''}%` })
      .orderBy(`course.${sort}`, `${order}`)
      .skip(page)
      .take(limit)
      .getManyAndCount();

    return { courses, count };
  }

  async create(dto: CreateCourseRequestDto): Promise<CreateCourseResponseDto> {
    const course = await this.coursesRepository.save(dto);

    return { ...course, studentsCount: 0 };
  }

  async findOneMarks(id: number): Promise<CoursesMarksResponseDto | object> {
    const course = await this.coursesRepository.findOneBy({ id });

    if (!course) return {};

    const marks = await this.coursesRepository
      .createQueryBuilder('courses')
      .select([
        'courses.name as course_name',
        'lector.name as lector_name',
        'student.name as student_name',
        'marks.mark as mark',
      ])
      .innerJoin('courses.marks', 'marks')
      .innerJoin('marks.lector', 'lector')
      .innerJoin('marks.student', 'student')
      .where('courses.id = :id', { id })
      .getRawMany();

    return marks;
  }

  async patch(
    id: number,
    dto: UpdateCourseRequestDto,
  ): Promise<UpdateCourseResponseDto> {
    const result = await this.coursesRepository.update(id, dto);

    if (!result.affected) throw new NotFoundException('Student not found');

    return await this.coursesRepository.findOneBy({ id });
  }

  async delete(id: number): Promise<void> {
    const result = await this.coursesRepository.delete(id);

    if (!result.affected) throw new NotFoundException('Student not found');
  }
}
