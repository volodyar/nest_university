import {
  Controller,
  UseGuards,
  Get,
  Post,
  Body,
  Query,
  UseFilters,
  HttpStatus,
  Param,
  Delete,
  Patch,
  HttpCode,
  UsePipes,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiTags,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiCookieAuth,
  ApiUnauthorizedResponse,
  ApiOperation,
  ApiNoContentResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { CoursesService } from './courses.service';
import { QueryFailedErrorFilter } from '../application/exception';
import { AuthGuard } from '../auth/guard';
import {
  CoursesMarksResponseDto,
  CreateCourseRequestDto,
  CreateCourseResponseDto,
  FindAllCoursesResponseDto,
  UpdateCourseRequestDto,
  UpdateCourseResponseDto,
} from './dto';
import { QueryFilterDto } from '../application/dto';
import { TrimPipe } from '../application/pipes';

@ApiTags('Courses')
@ApiCookieAuth()
@UseGuards(AuthGuard)
@UseFilters(QueryFailedErrorFilter)
@Controller('api/v1/courses')
export class CoursesCotroller {
  constructor(private readonly courseService: CoursesService) {}

  @ApiOperation({ summary: 'Get an array of courses' })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: FindAllCoursesResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/')
  findAll(@Query() query: QueryFilterDto): Promise<FindAllCoursesResponseDto> {
    return this.courseService.findAll(query);
  }

  @ApiOperation({ summary: 'Create the course' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: CreateCourseResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad Request | Credentials taken | Validation Error',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @UsePipes(new TrimPipe())
  @Post('/')
  create(
    @Body() dto: CreateCourseRequestDto,
  ): Promise<CreateCourseResponseDto> {
    return this.courseService.create(dto);
  }

  @ApiOperation({ summary: "Get the courses' marks" })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: [CoursesMarksResponseDto],
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/marks')
  findOneMarks(
    @Query('course') id: number,
  ): Promise<CoursesMarksResponseDto | object> {
    return this.courseService.findOneMarks(id);
  }

  @ApiOperation({ summary: "Update the course's data" })
  @ApiOkResponse({ type: UpdateCourseResponseDto })
  @ApiNotFoundResponse({ description: 'Not Found | Course not found' })
  @ApiBadRequestResponse({
    description: "Bad Request | Credentials don't exist | Validation error",
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  async patch(
    @Param('id') id: number,
    @Body() dto: UpdateCourseRequestDto,
  ): Promise<UpdateCourseResponseDto> {
    return await this.courseService.patch(id, dto);
  }

  @ApiOperation({ summary: 'Delete the course' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Not Found | Course not found' })
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id') id: number): Promise<void> {
    return await this.courseService.delete(id);
  }
}
