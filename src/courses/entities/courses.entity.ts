import { Entity, Column, ManyToMany, OneToMany, JoinTable } from 'typeorm';
import { CommonEntity } from '../../application/entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { Student } from '../../students/entities/students.entity';

@Entity({ name: 'courses' })
export class Course extends CommonEntity {
  @Column({ type: 'varchar', unique: true })
  name: string;

  @Column({ type: 'varchar' })
  description: string;

  @Column({ type: 'integer' })
  hours: number;

  @ManyToMany(() => Lector, (lector) => lector.courses, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    nullable: true,
  })
  lectors: [Lector];

  @ManyToMany(() => Student, (student) => student.courses, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'student_course',
    joinColumn: { name: 'course_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'student_id', referencedColumnName: 'id' },
  })
  students: [Student];

  @OneToMany(() => Mark, (mark) => mark.course, {
    cascade: true,
  })
  marks: Mark[];
}
