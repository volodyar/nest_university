import { ApiProperty } from '@nestjs/swagger';
import { CreateCourseResponseDto } from './create-course.response.dto';

export class FindAllCoursesResponseDto {
  @ApiProperty({ type: [CreateCourseResponseDto] })
  courses: CreateCourseResponseDto[];

  @ApiProperty({ default: 1 })
  count: number;
}
