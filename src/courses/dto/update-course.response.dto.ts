import { CreateCourseResponseDto } from './create-course.response.dto';

export class UpdateCourseResponseDto extends CreateCourseResponseDto {}
