import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
import { QueryFailedError } from 'typeorm';

@Catch(QueryFailedError)
export class QueryFailedErrorFilter implements ExceptionFilter {
  catch(exception: QueryFailedError, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    let message: string = 'Unknown error';
    let error = 'Bad Request';
    let status = HttpStatus.BAD_REQUEST;

    if (exception.driverError.code === '23505') {
      message = 'The data is already exists';

      if (exception.driverError.table === 'lectors') {
        status = HttpStatus.FORBIDDEN;
        error = 'Forbidden';
        message = 'Credentials taken';
      }

      if (['courses', 'groups'].includes(exception.driverError.table)) {
        message = 'The name is already exists';
      }

      if (exception.driverError.table === 'student_course') {
        message = 'The course is already added';
      }

      if (exception.driverError.table === 'students') {
        message = 'The email is already exists';
      }
    } else if (exception.driverError.code === '23503') {
      message = "The data doesn't exist, or execution is constrained";

      if (exception.driverError.table === 'lectors') {
        error = 'Forbidden';
        status = HttpStatus.FORBIDDEN;
        message = "Credentials don't exist";
      }
    } else {
      message = exception.message;
    }

    response.status(status).json({
      message,
      error,
      statusCode: status,
    });
  }
}
