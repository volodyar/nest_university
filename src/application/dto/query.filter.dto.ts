import { ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { order } from '../enums/order';

export class QueryFilterDto {
  @ApiPropertyOptional({ default: 'id' })
  @IsString()
  @IsOptional()
  public sort?: string = 'id';

  @ApiPropertyOptional({ enum: order, default: order.desc })
  @IsString()
  @IsEnum(order)
  @IsOptional()
  public order?: order = order.desc;

  @ApiPropertyOptional({ default: '1' })
  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  public offset?: number = 1;

  @ApiPropertyOptional({ default: '' })
  @IsString()
  @IsOptional()
  public name?: string;

  @ApiPropertyOptional({ default: '' })
  @IsString()
  @IsOptional()
  public group?: string;

  @ApiPropertyOptional({ default: '' })
  @IsString()
  @IsOptional()
  public course?: string;

  @ApiPropertyOptional({ default: '' })
  @IsString()
  @IsOptional()
  public surname?: string;

  @ApiPropertyOptional({ default: '10' })
  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  public limit?: number = 10;
}
