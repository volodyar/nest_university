import { ApiProperty } from '@nestjs/swagger';
import { CreateLectorResponseDto } from './create-lector.response.dto';

export class FindAllLectorsResponseDto {
  @ApiProperty({ type: [CreateLectorResponseDto] })
  lectors: CreateLectorResponseDto[];

  @ApiProperty({ default: 1 })
  count: number;
}
