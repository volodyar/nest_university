import { Type } from 'class-transformer';
import { IsNumber, Min } from 'class-validator';

export class addLectorCourseRequestDto {
  @IsNumber()
  @Min(1)
  @Type(() => Number)
  lectorId: number;

  @IsNumber()
  @Min(1)
  @Type(() => Number)
  courseId: number;
}
