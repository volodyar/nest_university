import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateLectorRequestDto } from './create-lector.request.dto';

export class UpdateLectoRequestDto extends PartialType(CreateLectorRequestDto) {
  @ApiProperty({ deprecated: true })
  password: string;
}
