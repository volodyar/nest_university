import { ApiProperty } from '@nestjs/swagger';
import { CreateLectorResponseDto } from './create-lector.response.dto';
import { StudentWithGroupDto } from '../../students/dto';

export class LectorStudentsResponseDto extends CreateLectorResponseDto {
  @ApiProperty({ type: [StudentWithGroupDto] })
  students: StudentWithGroupDto[];
}
