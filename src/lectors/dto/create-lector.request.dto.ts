import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  Length,
  Matches,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateLectorRequestDto {
  @ApiProperty({ default: 'johnsmith@gmail.com' })
  @IsEmail()
  @IsNotEmpty()
  @Length(2, 64)
  email: string;

  @ApiProperty({ default: 'John Smith' })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @Length(2, 64)
  name: string;

  @ApiProperty({ default: 'John Smith' })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @Length(2, 64)
  surname: string;

  @ApiProperty({ default: '123456789' })
  @IsString()
  @IsNotEmpty()
  @Length(6, 18)
  @Matches(/^[^\s]+$/, { message: 'Spaces are not allowed in password field' })
  password: string;
}
