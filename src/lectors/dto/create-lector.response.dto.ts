import { ApiProperty } from '@nestjs/swagger';
import { CreateLectorRequestDto } from './create-lector.request.dto';

export class CreateLectorResponseDto extends CreateLectorRequestDto {
  @ApiProperty({ default: 1 })
  id: number;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  createdAt: Date;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  updatedAt: Date;

  @ApiProperty({
    default: '$2b$10$rrpR4vxd/spcPPjQspJH9edsdonAsGeP0/.eNpqnmBLPWHczo4VU.',
  })
  password: string;
}
