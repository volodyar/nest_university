import { ApiProperty } from '@nestjs/swagger';
import { CreateCourseRequestDto } from '../../courses/dto';
import { CreateLectorResponseDto } from './create-lector.response.dto';

export class LectorCoursesResponseDto extends CreateLectorResponseDto {
  @ApiProperty({ type: [CreateCourseRequestDto] })
  courses: [CreateCourseRequestDto];
}
